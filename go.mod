module gitlab.com/nopadon.manlin/project-poc-updatepayload

go 1.12

require (
	github.com/coreos/etcd v3.3.22+incompatible // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/google/uuid v1.1.2
	github.com/openlyinc/pointy v1.1.2
	github.com/satori/go.uuid v1.2.0
	gitlab.com/ft25/iom/fm/model v0.0.9
	gitlab.com/ft25/iom/fm/utils v1.0.0
	gitlab.com/ft25/iom/framework v1.0.0
	gitlab.com/ft25/iom/model v1.0.0
	go.etcd.io/etcd v3.3.22+incompatible // indirect
)

replace (
	gitlab.com/ft25/iom/fm/model => gitlab.com/ft25/iom/fm/model.git v1.3.67
	gitlab.com/ft25/iom/fm/utils => gitlab.com/ft25/iom/fm/utils.git v1.0.25
	gitlab.com/ft25/iom/framework => gitlab.com/ft25/iom/framework.git v1.1.49
	gitlab.com/ft25/iom/model => gitlab.com/ft25/iom/model.git v1.0.22
)
