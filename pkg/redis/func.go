package redis

import (
	"strconv"

	"github.com/go-redis/redis"
	"gitlab.com/ft25/iom/fm/utils/lov"
	"gitlab.com/ft25/iom/framework/etcd"
)

var redisClient lov.IOMLov

func Connect(kvs etcd.KVSer) error {
	address, getKeyError := kvs.Get("redis/addr")
	if getKeyError != nil {
		return getKeyError
	}

	poolSize, getKeyError := kvs.Get("redis/pool")
	if getKeyError != nil {
		return getKeyError
	}

	maxRetries, getKeyError := kvs.Get("redis/max_retries")
	if getKeyError != nil {
		return getKeyError
	}

	db, getKeyError := kvs.Get("redis/lov_transform/db")
	if getKeyError != nil {
		return getKeyError
	}

	poolSizeInt, err := strconv.Atoi(string(poolSize))
	if err != nil {
		return err
	}

	maxRetriesInt, err := strconv.Atoi(string(maxRetries))
	if err != nil {
		return err
	}

	dbInt, err := strconv.Atoi(string(db))
	if err != nil {
		return err
	}

	redisOpt := redis.Options{
		Addr:       string(address),
		PoolSize:   poolSizeInt,
		MaxRetries: maxRetriesInt,
		DB:         dbInt,
	}

	redisClient = *new(lov.IOMLov)
	redisClient.Create(redisOpt)

	return nil
}

func Close() {
	redisClient.Close()
}
