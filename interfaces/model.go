package interfaces

type ElapsedTime struct {
	Cassandra  int    `json:"cassandra_elapsed_time,omitempty"`
	RabbitMQ   int    `json:"rabbitMQ_elapsed_time,omitempty"`
	JsonEncode int    `json:"json_encode_elapsed_time,omitempty"`
	XMLEncode  int    `json:"xml_encode_elapsed_time,omitempty"`
	Gval       int    `json:"gval_elapsed_time,omitempty"`
	Logger     int    `json:"logger_elapsed_time,omitempty"`
	Result     int    `json:"result_elapsed_time,omitempty"`
	Kafka      int    `json:"kafka_elapsed_time,omitempty"`
	ETCD       int    `json:"etcd_elapsed_time,omitempty"`
	Soap       int    `json:"soap_elapsed_time,omitempty"`
	Rest       int    `json:"rest_elapsed_time,omitempty"`
	Overall    int    `json:"overall_elapsed_time,omitempty"`
	QueueName  string `json:"queue_name,omitempty"`
}

type SubmitResponse struct {
	SubmitOrderResponse ResponseBody `json:"submitOrderResponse"`
}

type ResponseBody struct {
	TrackingID string `json:"tracking_id"`
	ResultCode int64  `json:"result_code"`
	ResultMsg  string `json:"result_message"`
}
