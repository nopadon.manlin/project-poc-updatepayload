package service

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
	payloadModel "gitlab.com/ft25/iom/fm/model/payload"
	"gitlab.com/ft25/iom/fm/utils/result"
	"gitlab.com/ft25/iom/fm/utils/timer"
	"gitlab.com/ft25/iom/framework/logger"
	"gitlab.com/ft25/iom/framework/rabbitmq"
	responseRepository "gitlab.com/ft25/iom/framework/repository/response"
	"gitlab.com/ft25/iom/framework/statuslist"
	"gitlab.com/ft25/iom/model/framework/order"
	"gitlab.com/ft25/iom/model/framework/orderflow"
	responseModel "gitlab.com/ft25/iom/model/framework/response"
	timerModel "gitlab.com/ft25/iom/model/framework/timer"
	"gitlab.com/nopadon.manlin/project-poc-updatepayload/interfaces"
)

func (a AsyncService) CreateData(ctx rabbitmq.Context, info order.Info) (stepStatus string, asyncID string, results []orderflow.Result, payload string, err error) {
	resultArr := result.New()
	createDataRequest := "CreateDataRequest"
	createDataResponse := "CreateDataResponse"
	elapsedTime := interfaces.ElapsedTime{QueueName: info.QueueName}
	overallElapsedTime := timer.ElapsedInMicroseconds()

	defer func() {
		elapsedTime.Overall = overallElapsedTime()
		_ = ctx.Logger.Print("debug", "fm-qrun", info.TrackingID, elapsedTime)
	}()

	errorProcessID := uuid.New().String()

	errorLogParams := logger.PrintTransactionParams{
		Level:           "error",
		Suffix:          "fm-poc",
		OperationName:   createDataRequest,
		OperationSystem: "poc",
		TrackingID:      info.TrackingID,
		OrderID:         info.OrderID,
		OrderType:       info.OrderType,
		OrderCreateDate: info.OrderCreateDate,
		OrderChannel:    info.OrderChannel,
		Status:          statuslist.FAILED,
		ProcessID:       &errorProcessID,
	}

	responseLogParams := logger.PrintResponseTransactionParams{
		Level:           "info",
		Suffix:          "fm-poc",
		OperationName:   createDataResponse,
		OperationSystem: "poc",
		TrackingID:      info.TrackingID,
		OrderID:         info.OrderID,
		OrderType:       info.OrderType,
		OrderCreateDate: info.OrderCreateDate,
		OrderChannel:    info.OrderChannel,
		Status:          statuslist.FAILED,
	}
	var payloadStruct payloadModel.Struct
	err = json.Unmarshal([]byte(info.Payload), &payloadStruct)
	if err != nil {
		failedResult := orderflow.Result{
			Code:          "500",
			Status:        statuslist.FAILED,
			BackendDetail: err.Error(),
		}

		resultElapsedTime := timer.ElapsedInMicroseconds()
		resultArr.AddResult(failedResult, errorProcessID)
		elapsedTime.Result += resultElapsedTime()

		loggerElapsedTime := timer.ElapsedInMicroseconds()
		errorLogParams.Error = err
		errorLogParams.Payload = resultArr.GetLastResult()
		_ = ctx.Logger.PrintTransaction(errorLogParams)
		elapsedTime.Logger += loggerElapsedTime()

		return statuslist.FAILED, "", resultArr.GetResultArray(), info.Payload, err
	}

	order := payloadStruct.Order
	isWorked := false

	productKey := *order.CustOrderID
	backendExtID := uuid.New().String()

	asyncResponseRepo := responseRepository.NewConnection(ctx.Session)
	asyncID = productKey

	ar := responseModel.Struct{
		AsyncID:    asyncID,
		StepID:     info.StepID,
		TrackingID: info.TrackingID,
		System:     "QRUN",
		Status:     statuslist.IN_PROGRESS,
		Total:      1,
		Result: []orderflow.Result{{
			Timestamp: GetTimestamp(timerModel.FormattedTime{Time: time.Now()}),
			Status:    statuslist.WAITING,
		}},
	}

	err = AsyncResponseStore(asyncResponseRepo, ar)

	if err != nil {
		failedResult := orderflow.Result{
			Code:           "500",
			OperationID:    *payloadStruct.Order.ExtID,
			Status:         statuslist.FAILED,
			BackendDetail:  err.Error(),
			BackendService: "TEST : POST",
		}

		resultElapsedTime := timer.ElapsedInMicroseconds()
		resultArr.AddResult(failedResult, backendExtID)
		elapsedTime.Result += resultElapsedTime()

		responseLogParams.Error = err
		responseLogParams.Payload = resultArr.GetLastResult()
		_ = ctx.Logger.PrintResponseTransaction(responseLogParams)

		return statuslist.FAILED, asyncID, resultArr.GetResultArray(), info.Payload, err
	}

	//Setup completed resultArr
	completedResult := orderflow.Result{
		ExtID:         backendExtID,
		Code:          "200",
		OperationID:   productKey,
		Status:        statuslist.COMPLETED,
		BackendDetail: "Complete",
	}
	resultArr.AddResult(completedResult, backendExtID)

	payloadJSON, _ := json.Marshal(payloadStruct)
	info.Payload = string(payloadJSON)

	if !isWorked {
		skipResult := orderflow.Result{
			Code:          "406",
			Status:        statuslist.SKIPPED,
			BackendDetail: "Skip by PreExecCheck",
		}
		resultArr.AddResult(skipResult, "")
		return statuslist.SKIPPED, "", resultArr.GetResultArray(), info.Payload, nil
	}

	return statuslist.COMPLETED, asyncID, resultArr.GetResultArray(), info.Payload, nil
	return statuslist.COMPLETED, asyncID, resultArr.GetResultArray(), info.Payload, nil
}
