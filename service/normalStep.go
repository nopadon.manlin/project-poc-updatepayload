package service

import (
	"encoding/json"

	uuid "github.com/satori/go.uuid"
	payloadModel "gitlab.com/ft25/iom/fm/model/payload"
	"gitlab.com/ft25/iom/fm/utils/result"
	"gitlab.com/ft25/iom/framework/rabbitmq"
	"gitlab.com/ft25/iom/framework/statuslist"
	"gitlab.com/ft25/iom/model/framework/order"
	"gitlab.com/ft25/iom/model/framework/orderflow"
)

func (s SequenceService) NormalStep(ctx rabbitmq.Context, info order.Info) (stepStatus string, results []orderflow.Result, payload string, err error) {

	r := result.New()

	var payloadStruct payloadModel.Struct
	_ = json.Unmarshal([]byte(info.Payload), &payloadStruct)
	completedResult := orderflow.Result{
		Code:          "200",
		OperationID:   *payloadStruct.Order.CustOrderID,
		Status:        statuslist.COMPLETED,
		BackendDetail: "Complete",
	}

	r.AddResult(completedResult, uuid.NewV4().String())
	return statuslist.COMPLETED, r.GetResultArray(), info.Payload, nil
}
