package service

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/openlyinc/pointy"
	payloadModel "gitlab.com/ft25/iom/fm/model/payload"
	"gitlab.com/ft25/iom/fm/utils/timer"
	"gitlab.com/ft25/iom/framework/rabbitmq"
	orderRepository "gitlab.com/ft25/iom/framework/repository/order"
	responseRepository "gitlab.com/ft25/iom/framework/repository/response"
	"gitlab.com/ft25/iom/framework/statuslist"
	"gitlab.com/ft25/iom/model/framework/order"
	"gitlab.com/ft25/iom/model/framework/orderflow"
	responseModel "gitlab.com/ft25/iom/model/framework/response"
	timerModel "gitlab.com/ft25/iom/model/framework/timer"
	"gitlab.com/nopadon.manlin/project-poc-updatepayload/interfaces"
)

func (a AsyncNotification) CreateDataNoti(ctx rabbitmq.Context, info order.NotificationInfo) (stepStatus string, waitStepID string, response responseModel.Struct, err error) {

	elapsedTime := interfaces.ElapsedTime{QueueName: info.QueueName}
	overallElapsedTime := timer.ElapsedInMicroseconds()

	defer func() {
		elapsedTime.Overall = overallElapsedTime()
		_ = ctx.Logger.Print("debug", "fm-qrun", info.TrackingID, elapsedTime)
	}()

	// payload to struct
	var payloadStruct payloadModel.Struct
	asyncResponseRepo := responseRepository.NewConnection(ctx.Session)

	ar, err := AsyncResponseFindByAsyncIDAndSystem(asyncResponseRepo, info.AsyncID, info.System)

	if err != nil {
		fmt.Print(err.Error())
		return statuslist.FAILED, waitStepID, response, nil
	}

	orderRepo := orderRepository.NewConnection(ctx.Session)
	o, err := OrderFindByTrackingID(orderRepo, info.TrackingID)

	if err != nil {
		ar.Status = statuslist.FAILED

		ar.Result = append(ar.Result, orderflow.Result{
			BackendDetail: err.Error(),
			Status:        statuslist.FAILED,
			Timestamp:     GetTimestamp(timerModel.FormattedTime{Time: time.Now()}),
		})

		arError := AsyncResponseUpdate(asyncResponseRepo, info.AsyncID, info.System, info.TrackingID, info.StepID, ar)
		if arError != nil {
			fmt.Print(arError.Error())
			return statuslist.FAILED, waitStepID, response, nil
		}
	}

	err = json.Unmarshal([]byte(o.Payload), &payloadStruct)

	if err != nil {
		ar.Status = statuslist.FAILED

		ar.Result = append(ar.Result, orderflow.Result{
			BackendDetail: err.Error(),
			Status:        statuslist.FAILED,
			Timestamp:     GetTimestamp(timerModel.FormattedTime{Time: time.Now()}),
		})

		arError := AsyncResponseUpdate(asyncResponseRepo, info.AsyncID, info.System, info.TrackingID, info.StepID, ar)

		if arError != nil {
			fmt.Print(arError.Error())
			return statuslist.FAILED, waitStepID, response, nil
		}
	}

	payloadStruct.Order.DealerName = pointy.String("test poc update payload")
	payloadB, err := json.Marshal(payloadStruct)
	err = OrderUpdatePayload(orderRepo, o.TrackingID, string(payloadB))
	if err != nil {
		fmt.Print(err.Error())
		return statuslist.FAILED, waitStepID, response, nil
	}

	return statuslist.COMPLETED, waitStepID, ar, nil
}
