package service

import (
	"gitlab.com/ft25/iom/framework/repository/counter"
	"gitlab.com/ft25/iom/framework/repository/order"
	"gitlab.com/ft25/iom/framework/repository/response"
	"gitlab.com/ft25/iom/framework/repository/suborder"
	counterModel "gitlab.com/ft25/iom/model/framework/counter"
	orderModel "gitlab.com/ft25/iom/model/framework/order"
	responseModel "gitlab.com/ft25/iom/model/framework/response"
	subOrderModel "gitlab.com/ft25/iom/model/framework/suborder"
)

var AsyncResponseStore = func(repo response.ResponseService, asyncResponse responseModel.Struct) error {
	return repo.Store(asyncResponse)
}

var AsyncResponseFindByAsyncIDAndSystem = func(repo response.ResponseService, asyncID string, system string) (responseModel.Struct, error) {
	return repo.FindByAsyncIDAndSystem(asyncID, system)
}

var AsyncResponseFindByAsyncIDAndSystemWithStatus = func(repo response.ResponseService, asyncID string, system string, status string) (responseModel.Struct, error) {
	return repo.FindByAsyncIDAndSystemWithStatus(asyncID, system, status)
}

var AsyncResponseUpdate = func(repo response.ResponseService, asyncID string, system string, trackingID string, stepID string, ar responseModel.Struct) error {
	return repo.Update(asyncID, system, trackingID, stepID, ar)
}

var OrderFindByTrackingID = func(repo order.OrderService, trackingID string) (orderModel.Struct, error) {
	return repo.FindByTrackingID(trackingID)
}

var OrderFindByOrderID = func(repo order.OrderService, orderID string) ([]orderModel.Struct, error) {
	return repo.FindByOrderID(orderID)
}

var OrderUpdatePayload = func(repo order.OrderService, trackingID string, payload string) error {
	return repo.UpdatePayload(trackingID, payload)
}

var CounterCount = func(repo counter.CounterService, trackingID string, stepID string) error {
	return repo.Count(trackingID, stepID)
}

var CounterFindByTrackingIDAndStepID = func(repo counter.CounterService, trackingID string, stepID string) (counterModel.Struct, error) {
	return repo.FindByTrackingIDAndStepID(trackingID, stepID)
}

var SubOrderCreate = func(repo suborder.SubOrderService, so subOrderModel.Struct) error {
	return repo.Create(so)
}
