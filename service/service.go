package service

import "gitlab.com/ft25/iom/framework/service"

type SequenceService struct {
	service.SequenceService
}

type AsyncService struct {
	service.AsyncService
}

type SubOrderService struct {
	service.SubOrderService
}

type AsyncNotification struct {
	service.AsyncNotification
}
