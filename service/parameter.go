package service

import (
	"fmt"
	"os"

	"gitlab.com/ft25/iom/fm/utils/request"
	timerModel "gitlab.com/ft25/iom/model/framework/timer"
)

var GetString = func(s string) string {
	return s
}

var GetTimestamp = func(timestamp timerModel.FormattedTime) timerModel.FormattedTime {
	return timestamp
}

var RequestCall = func(method string, url string, payload []byte, headers map[string]string) ([]byte, error) {
	return request.Call(method, url, payload, headers)
}

var GetSubmitOrderURL = func() string {
	env := os.Getenv("ENV")
	return fmt.Sprintf("http://%s-api-order.iom-%s.arctic.true.th/%s/api/order/submit_order", env, env, env)
}
