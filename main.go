package main

import (
	"fmt"

	"gitlab.com/ft25/iom/framework/service"
	"gitlab.com/nopadon.manlin/project-poc-updatepayload/pkg/redis"
	poc "gitlab.com/nopadon.manlin/project-poc-updatepayload/service"

	"gitlab.com/nopadon.manlin/project-poc-updatepayload/version"
)

func init() {
	forever := make(chan bool)

	ctx, closeConnection, err := service.InitConnection()
	if err != nil {
		panic(err)
	}

	defer closeConnection()

	err = redis.Connect(ctx.KVS)
	if err != nil {
		panic(err)
	}

	defer redis.Close()

	ctx.Logger.SetVersion(version.Number)

	err = service.Load(ctx, "poc", []interface{}{poc.SequenceService{}})
	if err != nil {
		panic(err)
	}

	err = service.Load(ctx, "poc", []interface{}{poc.AsyncService{}})
	if err != nil {
		panic(err)
	}

	err = service.Notify(ctx, "poc", []interface{}{poc.AsyncNotification{}})
	if err != nil {
		panic(err)
	}
	<-forever
}

func main() {
	fmt.Println("test")

	// publish data to redis
}
